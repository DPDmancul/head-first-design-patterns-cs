﻿using Ch07_Adapter;

WildTurkey turkey = new();
IDuck turkeyAsDuck = new TurkeyAdapter(turkey);

turkeyAsDuck.Quack();
turkeyAsDuck.Fly();