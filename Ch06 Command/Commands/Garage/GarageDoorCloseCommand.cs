namespace Ch06_Command.Commands.Garage;

using Ch06_Command.Controls;

public class GarageDoorCloseCommand
{
    private readonly GarageDoor _door;

    public GarageDoorCloseCommand(GarageDoor door) => _door = door;

    public void Execute() => _door.Down();

    public void Undo() => throw new NotImplementedException();
}