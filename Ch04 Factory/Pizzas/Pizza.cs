namespace Ch04_Factory.Pizzas;

using Ch04_Factory.Ingredients.Doughs;
using Ch04_Factory.Ingredients.Factories;
using Ch04_Factory.Ingredients.Sauces;

public abstract class Pizza
{
    private IDough _dough;
    private ISauce _sauce;
    
    protected IPizzaIngredientFactory PizzaIngredientFactory;
    
    public string Name { get; }

    protected Pizza(string name, IPizzaIngredientFactory pizzaIngredientFactory)
    {
        Name = name;
        PizzaIngredientFactory = pizzaIngredientFactory;
        _dough = PizzaIngredientFactory.CreateDough();
        _sauce = PizzaIngredientFactory.CreateSauce();
    }

    public virtual void Prepare()
    {
        Console.WriteLine($"Preparing {Name}");
        Console.WriteLine($"Tossing ${_dough}");
        Console.WriteLine($"Adding ${_sauce}");
    }

    public virtual void Bake() => Console.WriteLine("Bake for 25 minutes at 350");
    public virtual void Cut() => Console.WriteLine("Cutting the pizza into diagonal slices");
    public virtual void Box() => Console.WriteLine("Place pizza in official PizzaStore box");
}