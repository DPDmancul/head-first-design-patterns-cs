namespace Ch09_Composite.CompositeMenu;

public interface IMenuComponent
{
    string Name { get; }
    string Description { get; }

    void Print();

    // The following are reported in book but I think is not correct to put them here because are not common interface
    // If I would need them I would add a method returning a tagged union or similar 
    // double Price => throw new NotSupportedException();
    // bool Vegetarian => throw new NotSupportedException();
    // void Add(IMenuComponent component) => throw new NotSupportedException();
    // void Remove(IMenuComponent component) => throw new NotSupportedException();
    // IMenuComponent GetChild(int index) => throw new NotSupportedException();
}