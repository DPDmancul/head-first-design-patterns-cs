namespace Ch08_Template;

public class Tea : HotBeverage
{
    public override void AddMainIngredient() => Console.WriteLine("Steeping the tea");
    public override void AddCondiment() => Console.WriteLine("Adding lemon");

    public override void AfterPouringHook() => Console.WriteLine("Wait 15 minutes");
    
    public override bool CustomerWantsCondiments() => GetUserInput().ToLower().StartsWith("y");

    private static string GetUserInput()
    {
        Console.WriteLine("Would you like lemon with your tea (y/n)?");
        return Console.ReadLine() ?? string.Empty;
    }
}