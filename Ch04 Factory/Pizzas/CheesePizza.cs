namespace Ch04_Factory.Pizzas;

using Ch04_Factory.Ingredients.Cheese;
using Ch04_Factory.Ingredients.Factories;

public class CheesePizza : Pizza
{
    private ICheese _cheese;

    public CheesePizza(string name, IPizzaIngredientFactory pizzaIngredientFactory) : base(name,
        pizzaIngredientFactory)
    {
        _cheese = PizzaIngredientFactory.CreateCheese();
    }

    public override void Prepare()
    {
        base.Prepare();
        Console.WriteLine($"Adding {_cheese}");
    }
}