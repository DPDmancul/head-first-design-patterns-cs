namespace Ch06_Command.Controls;

public class CeilingFan
{
    public string Location { get; }
    public CeilingFanSpeed Speed { get; private set; }

    public CeilingFan(string location)
    {
        Location = location;
        Speed = CeilingFanSpeed.Off;
    }

    public void High()
    {
        Speed = CeilingFanSpeed.High;
        Console.WriteLine($"{Location} ceiling fan at high speed");
    }

    public void Medium()
    {
        Speed = CeilingFanSpeed.Medium;
        Console.WriteLine($"{Location} ceiling fan at medium speed");
    }

    public void Low()
    {
        Speed = CeilingFanSpeed.Low;
        Console.WriteLine($"{Location} ceiling fan at low speed");
    }

    public void Off()
    {
        Speed = CeilingFanSpeed.Off;
        Console.WriteLine($"{Location} ceiling fan powered off");
    }
}