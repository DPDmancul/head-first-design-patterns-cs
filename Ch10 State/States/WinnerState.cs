namespace Ch10_State.States;

public class WinnerState : SoldState
{
    public override void Dispense(GumballMachine gumballMachine) =>
        DoDispense(gumballMachine,
            afterNoQuarterStateHook: () =>
                DoDispense(gumballMachine,
                    afterDispensedHook: () =>
                        Console.WriteLine("YOU'RE A WINNER! You got two gumballs for your quarter")
                )
        );
}