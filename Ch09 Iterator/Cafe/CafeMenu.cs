namespace Ch09_Iterator.Cafe;

using Ch09_Iterator.Menu;

public class CafeMenu : IMenu
{
    private readonly IDictionary<string, MenuItem> _menuItems;
    
    public string Name { get; }

    public CafeMenu(string name)
    {
        _menuItems = new Dictionary<string, MenuItem>();
        Name = name;

        AddItem("Veggie Burger and Air Fries", "Veggie burger on a whole wheat bun, lettuce, tomato, and fries", true,
            3.99);
        AddItem("Soup of the day", "A cup of soup of the day, with a side salad", false, 3.69);
        AddItem("burrito", "A large burrito, with whole pinto beans, salsa, guacamole", true, 4.29);
    }

    public void AddItem(string name, string description, bool vegetarian, double price) =>
        _menuItems.Add(name, new MenuItem(name, description, vegetarian, price));

    public IEnumerator<MenuItem> GetEnumerator() => _menuItems.Values.GetEnumerator();
}