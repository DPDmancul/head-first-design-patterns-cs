namespace Ch10_State.States;

public interface IState
{
    void InsertQuarter(GumballMachine gumballMachine);
    void EjectQuarter(GumballMachine gumballMachine);
    bool TurnCrank(GumballMachine gumballMachine);
    void Dispense(GumballMachine gumballMachine);
    void Refill(GumballMachine gumballMachine)
    {
    }
}