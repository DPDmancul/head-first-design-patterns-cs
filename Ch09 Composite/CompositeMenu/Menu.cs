namespace Ch09_Composite.CompositeMenu;

using System.Collections;

public class Menu : IMenuComponent, IEnumerable<IMenuComponent>
{
    private readonly IList<IMenuComponent> _menuComponents;

    public string Name { get; }
    public string Description { get; }

    public Menu(string name, string description)
    {
        _menuComponents = new List<IMenuComponent>();
        Name = name;
        Description = description;
    }

    public void Add(IMenuComponent menuComponent) => _menuComponents.Add(menuComponent);
    public void Remove(IMenuComponent menuComponent) => _menuComponents.Remove(menuComponent);
    public IMenuComponent GetChild(int i) => _menuComponents[i];

    public void Print()
    {
        Console.WriteLine($"\n{Name}, {Description}");
        Console.WriteLine("--------------------");
        foreach (var menu in _menuComponents)
            menu.Print();
    }

    public IEnumerator<IMenuComponent> GetEnumerator() => _menuComponents.GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}