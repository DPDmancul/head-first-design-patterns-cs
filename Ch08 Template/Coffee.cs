namespace Ch08_Template;

public class Coffee : HotBeverage
{
    public override void AddMainIngredient() => Console.WriteLine("Dripping Coffee through filter");
    public override void AddCondiment() => Console.WriteLine("Adding sugar and milk");

    public override bool CustomerWantsCondiments() => GetUserInput().ToLower().StartsWith("y");

    private static string GetUserInput()
    {
        Console.WriteLine("Would you like milk and sugar with your coffee (y/n)?");
        return Console.ReadLine() ?? string.Empty;
    }
}