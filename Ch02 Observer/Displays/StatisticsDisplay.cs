namespace Ch02_Observer.Displays;

public class StatisticsDisplay : IObserver, IDisplayElement
{
    private double _min, _max, _sum;
    private int _n;
    private WeatherData _weatherData;

    public StatisticsDisplay(WeatherData weatherData)
    {
        _min = double.MaxValue;
        _max = double.MinValue;
        _weatherData = weatherData;
        _weatherData.RegisterObserver(this);
    }

    public void Update()
    {
        var temp = _weatherData.Temperature;
        _min = Math.Min(_min, temp);
        _max = Math.Max(_max, temp);
        _sum += temp;
        ++_n;
        Display();
    }

    public void Display() => Console.WriteLine($"Avg/Max/Min temperature = {_sum / _n}/{_max}/{_min}");
}