namespace Ch04_Factory.Ingredients.Factories;

using Ch04_Factory.Ingredients.Cheese;
using Ch04_Factory.Ingredients.Doughs;
using Ch04_Factory.Ingredients.Sauces;

public class ChicagoPizzaIngredientFactory : IPizzaIngredientFactory
{
    public IDough CreateDough() => new ThickCrustDough();
    public ICheese CreateCheese() => new MozzarellaCheese();
    public ISauce CreateSauce() => new PlumTomatoSauce();
}