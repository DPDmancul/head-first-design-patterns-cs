namespace Ch10_State.States;

public class SoldOutState : IState
{
    public void InsertQuarter(GumballMachine gumballMachine) => Console.WriteLine("The machine is sold out");
    public void EjectQuarter(GumballMachine gumballMachine) => Console.WriteLine("You haven't inserted a quarter yet");
    
    public bool TurnCrank(GumballMachine gumballMachine)
    {
        Console.WriteLine("There are no gumballs");
        return false;
    }

    public void Dispense(GumballMachine gumballMachine) => Console.WriteLine("No gumball dispensed");
    public void Refill(GumballMachine gumballMachine) => gumballMachine.CurrentState = GumballMachine.NoQuarterState;
}