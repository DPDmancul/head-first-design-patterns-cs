namespace Ch10_State.States;

public class SoldState : IState
{
    public void InsertQuarter(GumballMachine gumballMachine) =>
        Console.WriteLine("Please wait, we're already giving you a gumball");

    public void EjectQuarter(GumballMachine gumballMachine) => Console.WriteLine("Sorry, you already turned the crank");

    public bool TurnCrank(GumballMachine gumballMachine)
    {
        Console.WriteLine("Turning twice doesn't get you another gumball");
        return false;
    }

    protected static void DoDispense(GumballMachine gumballMachine, Action? afterDispensedHook = null,
        Action? afterNoQuarterStateHook = null)
    {
        gumballMachine.ReleaseBall();
        afterDispensedHook?.Invoke();
        if (gumballMachine.GumballsCount > 0)
        {
            gumballMachine.CurrentState = GumballMachine.NoQuarterState;
            afterNoQuarterStateHook?.Invoke();
        }
        else
        {
            Console.WriteLine("oops, out of gumballs!");
            gumballMachine.CurrentState = GumballMachine.SoldOutState;
        }
    }

    public virtual void Dispense(GumballMachine gumballMachine) => DoDispense(gumballMachine);
}