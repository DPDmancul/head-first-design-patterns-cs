namespace Ch06_Command.Commands.Stereo;

using Ch06_Command.Controls;

public class StereoOffCommand : ICommand
{
    private Stereo _stereo;

    public StereoOffCommand(Stereo stereo) => _stereo = stereo;

    public void Execute() => _stereo.Off();

    public void Undo() => throw new NotImplementedException();
}