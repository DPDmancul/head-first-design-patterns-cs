namespace Ch01_Strategy.QuackBehaviours;

public class Squeak : IQuackBehaviour
{
    public void Quack() => Console.WriteLine("Squeak");
}