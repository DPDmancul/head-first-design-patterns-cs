namespace Ch03_Decorator.Beverages;

public class HouseBlend : Beverage
{
    public HouseBlend() : base("House Blend Coffee")
    {
    }

    public override double Cost() => .89;
}