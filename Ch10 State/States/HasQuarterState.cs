namespace Ch10_State.States;

public class HasQuarterState : IState
{
    private Random _randomWinner;

    public HasQuarterState() => _randomWinner = new();

    public void InsertQuarter(GumballMachine gumballMachine) => Console.WriteLine("You can't insert another quarter");

    public void EjectQuarter(GumballMachine gumballMachine)
    {
        Console.WriteLine("Quarter returned");
        gumballMachine.CurrentState = GumballMachine.NoQuarterState;
    }

    public bool TurnCrank(GumballMachine gumballMachine)
    {
        Console.WriteLine("You turned ...");
        var winner = _randomWinner.Next(10);
        gumballMachine.CurrentState = (winner == 0 && gumballMachine.GumballsCount > 1)
            ? GumballMachine.WinnerState
            : GumballMachine.SoldState;
        return true;
    }

    public void Dispense(GumballMachine gumballMachine) => Console.WriteLine("No gumball dispensed");
}