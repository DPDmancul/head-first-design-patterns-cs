namespace Ch03_Decorator.Beverages;

public abstract class Beverage
{
    public virtual string Description { get; }

    protected Beverage() => Description = "Unknown beverage";
    protected Beverage(string description) => Description = description;

    public abstract double Cost();
}