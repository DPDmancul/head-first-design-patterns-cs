﻿using Ch06_Command;
using Ch06_Command.Commands;
using Ch06_Command.Commands.CeilingFan;
using Ch06_Command.Commands.Garage;
using Ch06_Command.Commands.Light;
using Ch06_Command.Commands.Stereo;
using Ch06_Command.Controls;

RemoteControl remoteControl = new();

Light livingRoomLight = new("Living Room");
Light kitchenLight = new("Kitchen");
CeilingFan ceilingFan = new("Living Room");
GarageDoor garageDoor = new("Garage");
Stereo stereo = new("Living Room");

LightOnCommand livingRoomLightOn = new(livingRoomLight);
LightOffCommand livingRoomLightOff = new(livingRoomLight);
LightOnCommand kitchenLightOn = new(kitchenLight);
LightOffCommand kitchenLightOff = new(kitchenLight);

CeilingFanCommand ceilingFanHigh = new(ceilingFan, CeilingFanSpeed.High);
CeilingFanCommand ceilingFanMedium = new(ceilingFan, CeilingFanSpeed.Medium);
CeilingFanCommand ceilingFanOff = new(ceilingFan, CeilingFanSpeed.Off);

GarageDoorOpenCommand garageDoorOpen = new(garageDoor);
GarageDoorCloseCommand garageCloseOpen = new(garageDoor);

StereoOnForCDCommand stereoOnWithCd = new(stereo);
StereoOffCommand stereoOff = new(stereo);

remoteControl.SetCommand(0, livingRoomLightOn, livingRoomLightOff);
remoteControl.SetCommand(1, kitchenLightOn, kitchenLightOff);
remoteControl.SetCommand(2, ceilingFanHigh, ceilingFanOff);
remoteControl.SetCommand(3, stereoOnWithCd, stereoOff);

Console.WriteLine(remoteControl);

for (var i = 0; i < 4; ++i)
{
    remoteControl.OnButtonWasPushed(i);
    remoteControl.OffButtonWasPushed(i);
}

// Undo
Console.WriteLine("\n\nUNDO");

remoteControl.OnButtonWasPushed(0);
remoteControl.OffButtonWasPushed(0);
Console.WriteLine(remoteControl);
remoteControl.UndoButtonWasPushed();
remoteControl.OffButtonWasPushed(0);
remoteControl.OnButtonWasPushed(0);
Console.WriteLine(remoteControl);
remoteControl.UndoButtonWasPushed();

// Undo
Console.WriteLine("\n\nUNDO FAN");

remoteControl.SetCommand(4, ceilingFanMedium, ceilingFanOff);
remoteControl.SetCommand(5, ceilingFanHigh, ceilingFanOff);

remoteControl.OnButtonWasPushed(4);
remoteControl.OffButtonWasPushed(4);
Console.WriteLine(remoteControl);
remoteControl.UndoButtonWasPushed();
remoteControl.OnButtonWasPushed(5);
remoteControl.UndoButtonWasPushed();

// Undo
Console.WriteLine("\n\nMACROS");

MacroCommand partyOnMacro = new(new ICommand[] {livingRoomLightOn, stereoOnWithCd});
MacroCommand partyOffMacro = new(new ICommand[] {livingRoomLightOff, stereoOff});
remoteControl.SetCommand(6, partyOnMacro, partyOffMacro);

Console.WriteLine(remoteControl);
Console.WriteLine("--- Pushing Macro On ---");
remoteControl.OnButtonWasPushed(6);
Console.WriteLine("--- Pushing Macro Off ---");
remoteControl.OffButtonWasPushed(6);
