namespace Ch03_Decorator.Beverages;

public class Espresso : Beverage
{
    public Espresso() : base("Espresso")
    {
    }

    public override double Cost() => 1.99;
}