namespace Ch06_Command.Commands.Light;

using Ch06_Command.Controls;

public class LightOnCommand : ICommand
{
    private readonly Light _light;

    public LightOnCommand(Light light) => _light = light;
    
    public void Execute() => _light.On();

    public void Undo() => _light.Off();
}