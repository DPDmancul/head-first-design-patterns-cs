namespace Ch02_Observer.Displays;

public class CurrentConditionsDisplay : IObserver, IDisplayElement
{
    private double _temp, _humidity, _pressure;
    private WeatherData _weatherData;

    public CurrentConditionsDisplay(WeatherData weatherData)
    {
        _weatherData = weatherData;
        _weatherData.RegisterObserver(this);
    }

    public void Update()
    {
        _temp = _weatherData.Temperature;
        _humidity = _weatherData.Humidity;
        _pressure = _weatherData.Pressure;
        Display();
    }

    public void Display() =>
        Console.WriteLine($"Current conditions: {_temp} °C, {_humidity}% humidity and {_pressure} bar pressure.");
}