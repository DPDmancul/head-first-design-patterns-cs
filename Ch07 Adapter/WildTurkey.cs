namespace Ch07_Adapter;

public class WildTurkey : ITurkey
{
    public void Gobble() => Console.WriteLine("Gobble gobble");
    public void Fly() => Console.WriteLine("Flying a short distance");
}