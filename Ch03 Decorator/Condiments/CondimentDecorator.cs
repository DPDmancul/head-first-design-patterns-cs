namespace Ch03_Decorator.Condiments;

using Ch03_Decorator.Beverages;

public abstract class CondimentDecorator : Beverage
{
    protected readonly Beverage Beverage;

    protected CondimentDecorator(Beverage beverage) => Beverage = beverage;

    public abstract override string Description { get; }
}