namespace Ch04_Factory.Stores;

using Ch04_Factory.Pizzas;

public interface IPizzaStore
{
    public Pizza? OrderPizza(string type)
    {
        var pizza = CreatePizza(type);

        pizza?.Prepare();
        pizza?.Bake();
        pizza?.Cut();
        pizza?.Box();

        return pizza;
    }

    Pizza? CreatePizza(string type);
}