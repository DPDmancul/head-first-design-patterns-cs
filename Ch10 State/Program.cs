﻿using Ch10_State;

GumballMachine gumballMachine = new(5);

foreach (var _ in Enumerable.Range(0, 6))
{
    gumballMachine.InsertQuarter();
    gumballMachine.TurnCrank();
    Console.WriteLine("---------------");
}

gumballMachine.Refill(1);
gumballMachine.InsertQuarter();
gumballMachine.TurnCrank();
