namespace Ch04_Factory.Ingredients.Factories;

using Ch04_Factory.Ingredients.Cheese;
using Ch04_Factory.Ingredients.Doughs;
using Ch04_Factory.Ingredients.Sauces;

public interface IPizzaIngredientFactory
{
    public IDough CreateDough();
    public ISauce CreateSauce();
    public ICheese CreateCheese();
}