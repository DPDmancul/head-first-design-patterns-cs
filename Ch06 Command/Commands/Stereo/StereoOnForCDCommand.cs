namespace Ch06_Command.Commands.Stereo;

using Ch06_Command.Controls;

public class StereoOnForCDCommand : ICommand
{
    private Stereo _stereo;

    public StereoOnForCDCommand(Stereo stereo) => _stereo = stereo;

    public void Execute()
    {
        _stereo.On();
        _stereo.SetCd();
        _stereo.SetVolume(11);
    }

    public void Undo() => throw new NotImplementedException();
}