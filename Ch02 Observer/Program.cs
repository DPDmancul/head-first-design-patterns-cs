﻿using Ch02_Observer;
using Ch02_Observer.Displays;

WeatherData weatherData = new();

CurrentConditionsDisplay currentDisplay = new(weatherData);
StatisticsDisplay statisticsDisplay = new(weatherData);
HeatIndexDisplay heatIndexDisplay = new(weatherData);

weatherData.SetMeasurements(26, 65, 1.01);
weatherData.SetMeasurements(27, 70, 1.02);
weatherData.SetMeasurements(25, 90, 1.009);
