﻿using Ch05_Singleton;

Console.WriteLine("Here we start");

var i1 = EnumSingleton.Instance;
i1.Greet();

var i2 = ClassSingleton.Instance;
i2.Greet();

var i3 = ClassSingleton.Instance;
i3.Greet();

var i4 = EnumSingleton.Instance;
i4.Greet();

var i5 = EnumSingleton.Instance;
i5.Greet();

var i6 = ClassSingleton.Instance;
i6.Greet();
