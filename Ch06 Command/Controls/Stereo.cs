namespace Ch06_Command.Controls;

public class Stereo
{
    public string Location { get; }
    
    public Stereo(string location) => Location = location ;

    public void On() => Console.WriteLine($"{Location} stereo powered on");
    public void Off() => Console.WriteLine($"{Location} stereo powered off");

    public void SetCd() => Console.WriteLine($"{Location} stereo in CD mode");
    public void SetDvd() => Console.WriteLine($"{Location} stereo in DVD mode");
    public void SetRadio() => Console.WriteLine($"{Location} stereo in Radio mode");

    public void SetVolume(uint volume) => Console.WriteLine($"{Location} stereo volume set to {volume}");
}