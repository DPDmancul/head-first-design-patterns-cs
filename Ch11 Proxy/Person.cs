namespace Ch11_Proxy;

public class Person : IPerson
{
    public string Name { get; set; }
    public string Gender { get; set; }
    public string Interests { get; set; }

    private int _ratingCount = 0;
    private int _ratingSum = 0;

    public int GeekRating
    {
        get => _ratingCount == 0 ? 0 : _ratingSum / _ratingCount;
        set
        {
            _ratingSum += value;
            ++_ratingCount;
        }
    }
    
    public Person(string name, string gender, string interests)
    {
        Name = name;
        Gender = gender;
        Interests = interests;
    }
}