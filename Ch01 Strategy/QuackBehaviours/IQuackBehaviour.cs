namespace Ch01_Strategy.QuackBehaviours;

public interface IQuackBehaviour
{
   void Quack();
}