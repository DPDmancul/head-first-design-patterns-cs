namespace Ch01_Strategy.QuackBehaviours;

public class Quacking : IQuackBehaviour
{
    public void Quack() => Console.WriteLine("Quack");
}