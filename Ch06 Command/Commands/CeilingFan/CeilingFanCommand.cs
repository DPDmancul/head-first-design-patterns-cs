namespace Ch06_Command.Commands.CeilingFan;

using Ch06_Command.Controls;

public class CeilingFanCommand : ICommand
{
    private readonly CeilingFan _fan;
    private readonly CeilingFanSpeed _speed;
    private CeilingFanSpeed _lastSpeed;

    public CeilingFanCommand(CeilingFan fan, CeilingFanSpeed speed)
    {
        _fan = fan;
        _speed = speed;
    }

    private void SetSpeed(CeilingFanSpeed speed)
    {
        switch (speed)
        {
            case CeilingFanSpeed.Off:
                _fan.Off();
                break;
            case CeilingFanSpeed.Low:
                _fan.Low();
                break;
            case CeilingFanSpeed.Medium:
                _fan.Medium();
                break;
            case CeilingFanSpeed.High:
                _fan.High();
                break;
        }
    }

    public void Execute()
    {
        _lastSpeed = _fan.Speed;
        SetSpeed(_speed);
    }

    public void Undo() => SetSpeed(_lastSpeed);
}