namespace Ch09_Iterator;

using Ch09_Iterator.Menu;

public class Waitress
{
    private readonly IMenu[] _menus;

    public Waitress(params IMenu[] menus) => _menus = menus;

    private static void PrintMenu(IEnumerable<MenuItem> menu)
    {
        foreach (var item in menu)
            item.Print();
    }

    private static void PrintMenu(IEnumerable<IMenu> menus) => PrintMenu(menus, menu => menu);
    private static void PrintMenu(IEnumerable<IMenu> menus, Func<IMenu,IEnumerable<MenuItem>> transform)
    {
        foreach (var menu in menus)
        {
            Console.WriteLine(menu.Name.ToUpper());
            PrintMenu(transform(menu));
            Console.WriteLine();
        }
    }

    public void PrintMenu()
    {
        Console.WriteLine("MENU\n----");
        PrintMenu(_menus);
    }

    public void PrintVegetarianMenu()
    {
        Console.WriteLine("VEGETARIAN MENU\n---------------");
        PrintMenu(_menus, menu => menu.Where(item => item.Vegetarian));
    }
}