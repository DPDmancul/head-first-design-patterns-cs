﻿using Ch04_Factory.Pizzas;
using Ch04_Factory.Stores;

IPizzaStore nyStore = new NYPizzaStore();
IPizzaStore chicagoStore = new ChicagoPizzaStore();

var pizza = nyStore.OrderPizza("cheese");
if (pizza is not null)
    Console.WriteLine($"Ethan ordered a {pizza.Name}");

pizza = chicagoStore.OrderPizza("cheese");
if (pizza is not null)
    Console.WriteLine($"Joel ordered a {pizza.Name}");