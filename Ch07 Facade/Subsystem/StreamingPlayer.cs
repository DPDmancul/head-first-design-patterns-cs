namespace Ch07_Facade.Subsystem;

public class StreamingPlayer
{
    private Amplifier _amplifier;

    public StreamingPlayer(Amplifier amplifier) => _amplifier = amplifier;

    public void On() => Console.WriteLine("Player on");
    public void Off() => Console.WriteLine("Player off");
    public void Play(string media) => Console.WriteLine($"Player playing \"{media}\"");
    public void Pause() => Console.WriteLine("Player pause");
    public void Stop() => Console.WriteLine("Player stop");
    public void SetSurroundAudio()
    {
        Console.WriteLine("Player surround");
        _amplifier.SetSurroundSound();
    }

    public void SetTwoChannelAudio()
    {
        Console.WriteLine("Player two channels");
        _amplifier.SetStereoSound();
    }
}