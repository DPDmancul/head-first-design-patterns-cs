namespace Ch03_Decorator.Beverages;

public class DarkRoast : Beverage
{
    public DarkRoast() : base("Dark Roast Coffee")
    {
    }

    public override double Cost() => .99;
}