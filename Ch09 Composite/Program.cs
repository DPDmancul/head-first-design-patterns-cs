﻿using Ch09_Composite;
using Ch09_Composite.CompositeMenu;

Menu pancakeHouseMenu = new("PANCAKE HOUSE MENU", "Breakfast")
{
    new MenuItem("K&B's Pancake Breakfast", "Pancakes with scrambled eggs and toast", true, 2.99),
    new MenuItem("Regular Pancake Breakfast", "Pancakes with fried eggs, sausage", false, 2.99),
    new MenuItem("Blueberry Pancakes", "Pancakes made with fresh blueberries", true, 3.49),
    new MenuItem("Waffles", "Waffles with your choice of blueberries or strawberries", true, 3.59)
};

Menu dinerMenu = new("DINER MENU", "Lunch")
{
    new MenuItem("Vegetarian BLT", "(Fakin') Bacon with lettuce & tomato on whole wheat", true, 2.99),
    new MenuItem("BLT", "Bacon with lettuce & tomato on whole wheat", false, 2.99),
    new MenuItem("Soup of the day", "Soup of the day, with a side of potato salad", false, 3.29),
    new MenuItem("Hotdog", "A hot dog, with sauerkraut, relish, onions, topped with cheese", false, 3.05),
    new MenuItem("Pasta", "Spaghetti with Marinara Sauce, and a slice of sourdough bread", true, 3.89)
};

Menu dessertMenu = new("DESSERT MENU", "Dessert of course!")
{
    new MenuItem("Apple Pie", "Apple pie with a flakey crust, topped with vanilla ice cream", true, 1.59),
    new MenuItem("Cheesecake", "Creamy New York cheesecake, with a chocolate graham crust", true, 1.99),
    new MenuItem("Sorbet", "A scoop of raspberry amd a scoop of lime", true, 1.89)
};

dinerMenu.Add(dessertMenu);

Menu cafeMenu = new("CAFE MENU", "Dinner")
{
    new MenuItem("Veggie Burger and Air Fries", "Veggie burger on a whole wheat bun, lettuce, tomato, and fries", true,
        3.99),
    new MenuItem("Soup of the day", "A cup of soup of the day, with a side salad", false, 3.69),
    new MenuItem("burrito", "A large burrito, with whole pinto beans, salsa, guacamole", true, 4.29)
};

Menu allMenus = new("ALL MENUS", "All menus combined")
{
    pancakeHouseMenu,
    dinerMenu,
    cafeMenu
};

Waitress waitress = new(allMenus);

waitress.PrintMenu();