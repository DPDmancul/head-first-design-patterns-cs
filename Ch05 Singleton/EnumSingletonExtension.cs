namespace Ch05_Singleton;

public static class EnumSingletonExtension
{
    public static void Greet(this EnumSingleton it) => Console.WriteLine($"Hello from EnumSingleton {it}");
}