namespace Ch09_Iterator.PancakeHouse;

using Ch09_Iterator.Menu;

public class PancakeHouseMenu : IMenu
{
    private readonly IList<MenuItem> _menuItems;
    
    public string Name { get; }

    public PancakeHouseMenu(string name)
    {
        _menuItems = new List<MenuItem>();
        Name = name;

        AddItem("K&B's Pancake Breakfast", "Pancakes with scrambled eggs and toast", true, 2.99);
        AddItem("Regular Pancake Breakfast", "Pancakes with fried eggs, sausage", false, 2.99);
        AddItem("Blueberry Pancakes", "Pancakes made with fresh blueberries", true, 3.49);
        AddItem("Waffles", "Waffles with your choice of blueberries or strawberries", true, 3.59);
    }

    public void AddItem(string name, string description, bool vegetarian, double price) =>
        _menuItems.Add(new MenuItem(name, description, vegetarian, price));

    public IEnumerator<MenuItem> GetEnumerator() => _menuItems.GetEnumerator();
}