namespace Ch09_Composite;

using Ch09_Composite.CompositeMenu;

public class Waitress
{
    private readonly IMenuComponent _allMenus;

    public Waitress(IMenuComponent allMenus) => _allMenus = allMenus;

    public void PrintMenu() => _allMenus.Print();
}