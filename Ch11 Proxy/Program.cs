﻿using Ch11_Proxy;

Person joe = new("Joe Javabean", "male", "computer");

IPerson ownerProxy = new OwnerProxy(joe);
Console.WriteLine($"Name is {ownerProxy.Name}");
ownerProxy.Interests = "Bowling, Go";
try
{
    ownerProxy.GeekRating = 10;
}
catch (Exception e)
{
    Console.WriteLine(e);
}

Console.WriteLine($"Rating is {ownerProxy.GeekRating}");

Console.WriteLine("-------------------------");

IPerson nonOwnerProxy = new NonOwnerProxy(joe);
Console.WriteLine($"Name is {nonOwnerProxy.Name}");
try
{
    nonOwnerProxy.Gender = "female";
}
catch (Exception e)
{
    Console.WriteLine(e);
}

nonOwnerProxy.GeekRating = 3;
Console.WriteLine($"Rating is {ownerProxy.GeekRating} ({nonOwnerProxy.GeekRating})");
nonOwnerProxy.GeekRating = 5;
Console.WriteLine($"Rating is {ownerProxy.GeekRating} ({nonOwnerProxy.GeekRating})");