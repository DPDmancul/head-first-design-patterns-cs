namespace Ch06_Command.Commands;

public class NoCommand : ICommand
{
    public void Execute()
    {
    }

    public void Undo()
    {
    }
}