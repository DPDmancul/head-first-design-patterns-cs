namespace Ch01_Strategy.FlyBehaviours;

public class FlyWithWings : IFlyBehaviour
{
    public void Fly() => Console.WriteLine("I'm flying!");
}