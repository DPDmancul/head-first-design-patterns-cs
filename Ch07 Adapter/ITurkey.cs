namespace Ch07_Adapter;

public interface ITurkey
{
    void Gobble();
    void Fly();
}