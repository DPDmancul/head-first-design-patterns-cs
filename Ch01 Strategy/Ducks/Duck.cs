namespace Ch01_Strategy.Ducks;

using Ch01_Strategy.FlyBehaviours;
using Ch01_Strategy.QuackBehaviours;

public abstract class Duck
{
   IFlyBehaviour _flyBehaviour;
   IQuackBehaviour _quackBehaviour;

   protected Duck(IFlyBehaviour flyBehaviour, IQuackBehaviour quackBehaviour)
   {
      _flyBehaviour = flyBehaviour;
      _quackBehaviour = quackBehaviour;
   }

   public void SetFlyBehaviour(IFlyBehaviour fb) => _flyBehaviour = fb;
   public void SetQuackBehaviour(IQuackBehaviour qb) => _quackBehaviour = qb;

   public void PerformFly() => _flyBehaviour.Fly();
   
   public void PerformQuack() => _quackBehaviour.Quack();

   public void Swim() => Console.WriteLine("All ducks float, even decoy");
   
   public abstract void Display();
}