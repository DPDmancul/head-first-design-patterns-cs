namespace Ch06_Command.Controls;

public class Light
{
    public string Location { get; }

    public Light(string location) => Location = location;

    public void On() => Console.WriteLine($"{Location} light powered on");
    public void Off() => Console.WriteLine($"{Location} light powered off");
}