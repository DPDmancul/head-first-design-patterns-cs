namespace Ch07_Adapter;

public class TurkeyAdapter : IDuck
{
    private readonly ITurkey _adaptee;

    public TurkeyAdapter(ITurkey turkey) => _adaptee = turkey;

    public void Quack() => _adaptee.Gobble();

    public void Fly()
    {
        foreach (var _ in Enumerable.Range(0, 5))
            _adaptee.Fly();
    }
}