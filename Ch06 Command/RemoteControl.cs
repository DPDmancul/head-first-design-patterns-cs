namespace Ch06_Command;

using System.Security.Cryptography;
using System.Text;
using Ch06_Command.Commands;

public class RemoteControl
{
    private readonly ICommand[] _onCommands, _offCommands;
    private ICommand _undoCommand;
    private const int Slots = 7;

    public RemoteControl()
    {
        var noCommand = new NoCommand();
        _undoCommand = noCommand;
        _onCommands = new ICommand[Slots];
        _offCommands = new ICommand[Slots];
        for (var i = 0; i < Slots; ++i)
            _onCommands[i] = _offCommands[i] = noCommand;
    }

    private static bool IsSlotValid(int slot) => slot is >= 0 and < Slots;

    public void SetCommand(int slot, ICommand onCommand, ICommand offCommand)
    {
        if (IsSlotValid(slot))
        {
            _onCommands[slot] = onCommand;
            _offCommands[slot] = offCommand;
        }
    }

    private void ExecuteCmd(ICommand cmd)
    {
        cmd.Execute();
        _undoCommand = cmd;
    }

    public void OnButtonWasPushed(int slot)
    {
        if (IsSlotValid(slot))
            ExecuteCmd(_onCommands[slot]);
    }

    public void OffButtonWasPushed(int slot)
    {
        if (IsSlotValid(slot))
            ExecuteCmd(_offCommands[slot]);
    }

    public void UndoButtonWasPushed()
    {
        _undoCommand.Undo();
        _undoCommand = new NoCommand();
    }

    public override string ToString()
    {
        StringBuilder str = new();
        str.Append("\n------ Remote Control ------\n");
        for (int i = 0; i < Slots; ++i)
            str.Append($"[slot {i}] {_onCommands[i].GetType().Name,-25} {_offCommands[i].GetType().Name}\n");
        str.Append($"[undo] {_undoCommand.GetType().Name}\n");
        return str.ToString();
    }
}