namespace Ch03_Decorator.Condiments;

using Ch03_Decorator.Beverages;

public class Whip : CondimentDecorator
{
    public Whip(Beverage beverage) : base(beverage)
    {
    }

    public override string Description => $"{Beverage.Description}, Whip";
    public override double Cost() => Beverage.Cost() + .10;
}