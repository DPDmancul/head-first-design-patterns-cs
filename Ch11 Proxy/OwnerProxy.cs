namespace Ch11_Proxy;

public class OwnerProxy : IPerson
{
    private readonly IPerson _person;

    public OwnerProxy(IPerson person) => this._person = person;

    public string Name
    {
        get => _person.Name;
        set => _person.Name = value;
    }

    public string Gender
    {
        get => _person.Gender;
        set => _person.Gender = value;
    }

    public string Interests
    {
        get => _person.Interests;
        set => _person.Interests = value;
    }

    public int GeekRating
    {
        get => _person.GeekRating;
        set => throw new MemberAccessException();
    }
}