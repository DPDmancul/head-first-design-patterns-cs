namespace Ch09_Iterator.Diner;

using Ch09_Iterator.Menu;

public class DinerMenu : IMenu
{
    private const int MaxItems = 6;
    private readonly MenuItem?[] _menuItems;
    private int _i = 0;
    
    public string Name { get; }

    public DinerMenu(string name)
    {
        _menuItems = new MenuItem?[MaxItems];
        Name = name;

        AddItem("Vegetarian BLT", "(Fakin') Bacon with lettuce & tomato on whole wheat", true, 2.99);
        AddItem("BLT", "Bacon with lettuce & tomato on whole wheat", false, 2.99);
        AddItem("Soup of the day", "Soup of the day, with a side of potato salad", false, 3.29);
        AddItem("Hotdog", "A hot dog, with sauerkraut, relish, onions, topped with cheese", false, 3.05);
    }

    public void AddItem(string name, string description, bool vegetarian, double price) =>
        _menuItems[_i++] = new MenuItem(name, description, vegetarian, price);

    public IEnumerator<MenuItem> GetEnumerator() => new DinerMenuIterator(_menuItems);
}