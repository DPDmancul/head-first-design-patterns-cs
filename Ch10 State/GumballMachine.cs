namespace Ch10_State;

using Ch10_State.States;

public class GumballMachine
{
    internal static NoQuarterState NoQuarterState { get; } = new();
    internal static HasQuarterState HasQuarterState { get; } = new();
    internal static SoldState SoldState { get; } = new();
    internal static SoldOutState SoldOutState { get; } = new();
    internal static WinnerState WinnerState { get; } = new();

    internal IState CurrentState { get; set; }
    internal uint GumballsCount { get; private set; }

    public GumballMachine(uint numberGumballs)
    {
        GumballsCount = numberGumballs;
        CurrentState = numberGumballs > 0 ? NoQuarterState : SoldOutState;
    }

    public void InsertQuarter() => CurrentState.InsertQuarter(this);
    public void EjectQuarter() => CurrentState.EjectQuarter(this);
    public void TurnCrank()
    {
        if (CurrentState.TurnCrank(this))
            CurrentState.Dispense(this);
    }

    public void Refill(uint amount)
    {
        GumballsCount += amount;
        Console.WriteLine($"The gumball machine was just refilled; its new count is {GumballsCount}");
        CurrentState.Refill(this);
    }

    internal void ReleaseBall()
    {
        if (GumballsCount > 0)
        {
            Console.WriteLine("A gumball comes rolling out the slot...");
            GumballsCount -= 1;
        }
    }
}