namespace Ch01_Strategy.FlyBehaviours;

public interface IFlyBehaviour
{
    void Fly();
}