namespace Ch10_State.States;

public class NoQuarterState : IState
{
    public void InsertQuarter(GumballMachine gumballMachine)
    {
        Console.WriteLine("You inserted a quarter");
        gumballMachine.CurrentState = GumballMachine.HasQuarterState;
    }

    public void EjectQuarter(GumballMachine gumballMachine) => Console.WriteLine("You haven't inserted a quarter");
    
    public bool TurnCrank(GumballMachine gumballMachine)
    {
        Console.WriteLine("You turned but there's  no quarter");
        return false;
    }

    public void Dispense(GumballMachine gumballMachine) => Console.WriteLine("You need to pay first");
}