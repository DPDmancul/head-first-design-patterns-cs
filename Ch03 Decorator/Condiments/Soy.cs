namespace Ch03_Decorator.Condiments;

using Ch03_Decorator.Beverages;

public class Soy : CondimentDecorator
{
    public Soy(Beverage beverage) : base(beverage)
    {
    }

    public override string Description => $"{Beverage.Description}, Soy";
    public override double Cost() => Beverage.Cost() + .15;
}