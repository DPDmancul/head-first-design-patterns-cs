namespace Ch06_Command.Controls;

public class GarageDoor
{
    public string Location { get; }
    
    public GarageDoor(string location) => Location = location ;

    public void Up() => Console.WriteLine($"{Location} garage door opened");
    public void Down() => Console.WriteLine($"{Location} garage door closed");
    public void Stop() => Console.WriteLine($"{Location} garage door stopped");

    public void LightOn() => Console.WriteLine($"{Location} garage door light powered on");
    public void LightOff() => Console.WriteLine($"{Location} garage door light powered off");
}