namespace Ch07_Adapter;

public interface IDuck
{
    void Quack();
    void Fly();
}