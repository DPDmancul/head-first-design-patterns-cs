namespace Ch07_Facade.Subsystem;

public class Tuner
{
    private Amplifier _amplifier;

    public Tuner(Amplifier amplifier) => _amplifier = amplifier;

    public void On() => Console.WriteLine("Tuner on");
    public void Off() => Console.WriteLine("Tuner off");
    public void SetAm() => Console.WriteLine("Tuner AM");
    public void SetFm() => Console.WriteLine("Tuner FM");
    public void SetFrequency(float frequency) => Console.WriteLine($"Tuner on {frequency:N2} MHz");
}