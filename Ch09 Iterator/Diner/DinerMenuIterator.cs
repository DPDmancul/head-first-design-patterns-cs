namespace Ch09_Iterator.Diner;

using System.Collections;
using Ch09_Iterator.Menu;

public class DinerMenuIterator : IEnumerator<MenuItem>
{
    public MenuItem Current => _menuItems[_i]!;
    object IEnumerator.Current => Current;

    private readonly MenuItem?[] _menuItems;
    private int _i;

    public DinerMenuIterator(MenuItem?[] menuItems)
    {
        _menuItems = menuItems;
        Reset();
    }

    public bool MoveNext() => ++_i < _menuItems.Length && _menuItems[_i] is not null;
    public void Reset() => _i = -1;


    public void Dispose()
    {
    }
}