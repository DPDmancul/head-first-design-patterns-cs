namespace Ch03_Decorator.Condiments;

using Ch03_Decorator.Beverages;

public class Mocha : CondimentDecorator
{
    public Mocha(Beverage beverage) : base(beverage)
    {
    }

    public override string Description => $"{Beverage.Description}, Mocha";
    public override double Cost() => Beverage.Cost() + .20;
}