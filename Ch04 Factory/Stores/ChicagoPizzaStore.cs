namespace Ch04_Factory.Stores;

using Ch04_Factory.Ingredients.Factories;
using Ch04_Factory.Pizzas;

public class ChicagoPizzaStore : IPizzaStore
{
    public Pizza? CreatePizza(string type)
    {
        var factory = new ChicagoPizzaIngredientFactory();
        return type switch
        {
            "cheese" => new CheesePizza("ChicagoStyleCheesePizza", factory),
            _ => null
        };
    }
}