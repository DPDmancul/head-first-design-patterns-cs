namespace Ch04_Factory.Ingredients.Factories;

using Ch04_Factory.Ingredients.Cheese;
using Ch04_Factory.Ingredients.Doughs;
using Ch04_Factory.Ingredients.Sauces;

public class NYPizzaIngredientFactory : IPizzaIngredientFactory
{
    public IDough CreateDough() => new ThinCustDough();
    public ICheese CreateCheese() => new ReggianoCheese();
    public ISauce CreateSauce() => new MarinaraSauce();
}