namespace Ch06_Command.Commands;

public class MacroCommand : ICommand
{
    private readonly List<ICommand> _commands;

    public MacroCommand(IEnumerable<ICommand> commands) => _commands = commands.ToList();

    public void Execute()
    {
        foreach (var cmd in _commands)
            cmd.Execute();
    }

    public void Undo()
    {
        foreach (var cmd in Enumerable.Reverse(_commands))
            cmd.Undo();
    }
}