namespace Ch02_Observer;

public interface IObserver
{
    void Update();
}