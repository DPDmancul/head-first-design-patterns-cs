﻿using Ch07_Facade;
using Ch07_Facade.Subsystem;

Amplifier amp = new();
Tuner tuner = new(amp);
StreamingPlayer player = new(amp);
Projector projector = new(player);
Screen screen = new();
TheatreLights lights = new();
PopcornPopper popper = new();

HomeTheaterFacade homeTheater = new(amp, tuner, player, projector, screen, lights, popper);

homeTheater.WatchMovie("Raiders of the Lost Ark");
homeTheater.EndMovie();