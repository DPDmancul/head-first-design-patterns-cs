namespace Ch06_Command.Controls;

public enum CeilingFanSpeed
{
    Off,
    Low,
    Medium,
    High
}