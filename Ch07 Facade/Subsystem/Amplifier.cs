namespace Ch07_Facade.Subsystem;

public class Amplifier
{
    private Tuner? _tuner;
    private StreamingPlayer? _player;

    public void SetStreamingPlayer(StreamingPlayer player) => _player = player;
    public void SetTuner(Tuner tuner) => _tuner = tuner;
    
    public void On() => Console.WriteLine("Amplifier on");
    public void Off() => Console.WriteLine("Amplifier off");
    public void SetStereoSound() => Console.WriteLine("Amplifier stereo");
    public void SetSurroundSound() => Console.WriteLine("Amplifier surround");
    public void SetVolume(int volume) => Console.WriteLine($"Amplifier volume set to {volume}");
}