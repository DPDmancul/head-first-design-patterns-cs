﻿using Ch01_Strategy.Ducks;
using Ch01_Strategy.FlyBehaviours;

Duck mallard = new MallardDuck();
mallard.Display();
mallard.PerformQuack();
mallard.PerformFly();

Duck model = new ModelDuck();
model.Display();
model.PerformFly();
model.SetFlyBehaviour(new FlyRocketPowered());
model.PerformFly();