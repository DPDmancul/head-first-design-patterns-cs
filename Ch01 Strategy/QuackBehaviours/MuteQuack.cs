namespace Ch01_Strategy.QuackBehaviours;

public class MuteQuack : IQuackBehaviour
{
   public void Quack() => Console.WriteLine("<< Silence >>");
}