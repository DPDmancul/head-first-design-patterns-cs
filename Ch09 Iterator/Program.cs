﻿using Ch09_Iterator;
using Ch09_Iterator.Cafe;
using Ch09_Iterator.Diner;
using Ch09_Iterator.PancakeHouse;

PancakeHouseMenu pancakeHouseMenu = new("Breakfast");
DinerMenu dinerMenu = new("Lunch");
CafeMenu cafeMenu = new("Dinner");

Waitress waitress = new(pancakeHouseMenu, dinerMenu, cafeMenu);

waitress.PrintMenu();
waitress.PrintVegetarianMenu();
