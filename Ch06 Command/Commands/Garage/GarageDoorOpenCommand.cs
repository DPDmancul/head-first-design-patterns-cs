namespace Ch06_Command.Commands.Garage;

using Ch06_Command.Controls;

public class GarageDoorOpenCommand
{
    private readonly GarageDoor _door;

    public GarageDoorOpenCommand(GarageDoor door) => _door = door;

    public void Execute() => _door.Up();

    public void Undo() => throw new NotImplementedException();
}