namespace Ch08_Template;

public abstract class HotBeverage
{
    public void PrepareReceipt()
    {
        BoilWater();
        AddMainIngredient();
        PourInCup();
        AfterPouringHook();
        if (CustomerWantsCondiments())
            AddCondiment();
    }

    public void BoilWater() => Console.WriteLine("Boiling water");
    public void PourInCup() => Console.WriteLine("Pouring into cup");

    public abstract void AddMainIngredient();
    public abstract void AddCondiment();

    public virtual void AfterPouringHook()
    {
    }

    public virtual bool CustomerWantsCondiments() => true;
}