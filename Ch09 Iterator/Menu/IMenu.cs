namespace Ch09_Iterator.Menu;

using System.Collections;

public interface IMenu : IEnumerable<MenuItem>
{
    public string Name { get; }

    public abstract void AddItem(string name, string description, bool vegetarian, double price);
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}