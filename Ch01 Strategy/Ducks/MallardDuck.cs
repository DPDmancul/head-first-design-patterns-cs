namespace Ch01_Strategy.Ducks;

using Ch01_Strategy.FlyBehaviours;
using Ch01_Strategy.QuackBehaviours;

public class MallardDuck : Duck
{
    public MallardDuck() : base(new FlyWithWings(), new Quacking())
    {
    }

    public override void Display() => Console.WriteLine("I'm a real Mallard Duck");
}