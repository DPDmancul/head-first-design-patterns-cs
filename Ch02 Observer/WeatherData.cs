namespace Ch02_Observer;

public class WeatherData : ISubject
{
    private readonly HashSet<IObserver> _observers;

    public double Temperature { get; private set; }
    public double Humidity { get; private set; }
    public double Pressure { get; private set; }

    public WeatherData()
    {
        _observers = new();
    }

    private void MeasurementsChanged() => NotifyObservers();
    
    public void RegisterObserver(IObserver o) => _observers.Add(o);
    
    public void RemoveObserver(IObserver o) => _observers.Remove(o);

    public void NotifyObservers()
    {
        foreach (var o in _observers)
            o.Update();
    }

    public void SetMeasurements(double temperature, double humidity, double pressure)
    {
        Temperature = temperature;
        Humidity = humidity;
        Pressure = pressure;
        MeasurementsChanged();
    }
}