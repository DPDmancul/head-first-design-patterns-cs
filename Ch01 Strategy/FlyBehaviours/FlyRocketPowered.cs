namespace Ch01_Strategy.FlyBehaviours;

public class FlyRocketPowered : IFlyBehaviour
{
    public void Fly() => Console.WriteLine("I'm flying with a rocket!");
}