namespace Ch01_Strategy.Ducks;

using Ch01_Strategy.FlyBehaviours;
using Ch01_Strategy.QuackBehaviours;

public class ModelDuck : Duck
{
    public ModelDuck() : base(new FlyNoWay(), new Quacking())
    {
    }

    public override void Display() => Console.WriteLine("I'm a model duck");
}