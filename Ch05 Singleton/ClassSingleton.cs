namespace Ch05_Singleton;

public class ClassSingleton
{
    public static ClassSingleton Instance { get; }

    private ClassSingleton()
    {
        Console.WriteLine("ClassSingleton created");
    }

    static ClassSingleton()
    {
        // static constructor is run only once: no need for synchronization
        Instance = new();
    }

    public void Greet() => Console.WriteLine($"Hello from ClassSingleton {this}");
}