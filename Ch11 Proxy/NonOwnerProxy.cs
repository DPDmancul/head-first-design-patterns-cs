namespace Ch11_Proxy;

public class NonOwnerProxy : IPerson
{
    private readonly IPerson _person;

    public NonOwnerProxy(IPerson person) => _person = person;

    public string Name
    {
        get => _person.Name;
        set => throw new MemberAccessException();
    }

    public string Gender
    {
        get => _person.Gender;
        set => throw new MemberAccessException();
    }

    public string Interests
    {
        get => _person.Interests;
        set => throw new MemberAccessException();
    }

    public int GeekRating
    {
        get => _person.GeekRating;
        set => _person.GeekRating = value;
    }
}