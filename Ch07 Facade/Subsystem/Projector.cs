namespace Ch07_Facade.Subsystem;

public class Projector
{
    private StreamingPlayer _player;

    public Projector(StreamingPlayer player) => _player = player;

    public void On() => Console.WriteLine("Projector on");
    public void Off() => Console.WriteLine("Projector off");
    public void TvMode() => Console.WriteLine("Projector in TV mode");
    public void WideScreenMode() => Console.WriteLine("Projector in wide screen mode");
}