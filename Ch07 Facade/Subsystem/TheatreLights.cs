namespace Ch07_Facade.Subsystem;

public class TheatreLights
{
    public void On() => Console.WriteLine("Theatre lights on");
    public void Off() => Console.WriteLine("Theatre lights off");
    public void Dim(int intensity) => Console.WriteLine($"Theatre lights at {intensity}%");
}