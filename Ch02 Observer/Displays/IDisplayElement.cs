namespace Ch02_Observer.Displays;

public interface IDisplayElement
{
    void Display();
}